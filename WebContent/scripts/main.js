let stop = false, textFile = null;
let type = 'random';
let alg = window.location.href.match(/([^\/]+)\.html$/);
if(alg && alg[1] != 'index'){
	type = alg[1];
}
let VUE_APP_BASE_URL = window.location.href.replace(/\/[^\/]+\.html$/, '');
if(VUE_APP_BASE_URL.lastIndexOf('/')== VUE_APP_BASE_URL.length - 1){
	VUE_APP_BASE_URL = VUE_APP_BASE_URL.substring(0, VUE_APP_BASE_URL.length - 1);
}
let VUE_APP_BASE_API_URL = VUE_APP_BASE_URL + '/api/';

var app = new Vue({
    el: '#app',
    data: {
    	noOfNodes: type == 'pattern' ? 100 : 10,
    	noOfLevels: 5,
    	modified: 0.10,
    	added: 1,
    	deleted: 1,
    	pattern: [{id: 0, value: 32}, {id: 1, value: 50}, {id: 2, value: 10}, {id: 3, value: 5}, {id: 4, value: 3}],
    	graphData : localStorage.graphData != undefined ? JSON.parse(localStorage.graphData) : { random: null, pattern: null, distort: null, help: true}, //"digraph G { B -> G; }",
    	layout: "dot",
    	//generateData : localStorage.graphData,//"digraph G { B -> G; }",
    	file: '',
    	type: type
    },
  computed: {
	  renderedInput: function(event){
			//return new Viz().renderSVGElement
		    let data = null;
		  	if(this.graphData != undefined && this.graphData[this.type] != undefined &&
		  			this.graphData[this.type].input != undefined ){
	    		data = this.graphData[this.type].input;
	    	}
			if(data != null && data.viz != undefined){
				//return Viz(this.generateData);
				this.addSvgPlugin(false);
				return Viz(data.viz);
			} else {
				return "";
			}
	  },
	  renderInfoInput: function(event){
	    	let info = 'No input';
	    	let data = null;
	    	if(this.graphData != undefined && this.graphData[this.type] != undefined &&
		  			this.graphData[this.type].input != undefined ){
	    		data = this.graphData[this.type].input;
	    	}
	    	if(data != null){
	    		info = 'Input: ';
	    		if(data.file != undefined){
	    			info += data.file + ', ';
	    		}
	    		info += data.type + ', ' + data.time + ', ';
	    		info += this.getParams(data);
	    		/*if(data.params != undefined && data.params.noOfNodes != undefined){
	    			info += ', size: ' + data.params.noOfNodes; 
	    		}*/
	    	} 
	    	return info;
	    },
	  renderedOutput: function(event){
  			//return new Viz().renderSVGElement
		    let data = null;
		  	if(this.graphData != undefined && this.graphData[this.type] != undefined &&
		  			this.graphData[this.type].output != undefined ){
	    		data = this.graphData[this.type].output;
	    	}
		  	
  			if(data != null && data.viz != undefined){
  				//return Viz(this.generateData);
  				this.addSvgPlugin(true);
  				return Viz(data.viz);
  			} else {
  				return "";
  			}
    },
    renderInfoOutput: function(event){
    	let info = '';
    	let data = null;
    	if(this.graphData != undefined && this.graphData[this.type] != undefined  &&
	  			this.graphData[this.type].output != undefined ){
    		data = this.graphData[this.type].output;
    	}
    	if(data != null){
    		info = 'Output: ';
    		info += data.type + ', ' + data.time + ', ';
    		info += this.getParams(data);
    		/*if(data.params.noOfNodes != undefined){
    			info += ', size: ' + data.params.noOfNodes; 
    		}*/
    	} 
    	return info;
    },
    questionTitle: function(event){
    	return 'Turn help ' + (this.graphData.help ? 'off' : 'on');
    }
  },
  // define methods under the `methods` object
  methods: {
	handleFileUpload(){
		 this.file = this.$refs.file.files[0];
		 let reader = new FileReader();
		 let mod = this;
		 reader.onload = function(){
			 mod.graphData[type] = { input: { type: 'file', data: reader.result, viz: reader.result, file: mod.file.name, time: mod.file.lastModifiedDate }, output: null};
			 localStorage.graphData = JSON.stringify(mod.graphData);
		 };
		 reader.readAsText(this.file);
		 
	},
	download: function(event){
	    var data = new Blob([this.graphData[type].output.data], {type: 'text/plain'});
	    // If we are replacing a previously generated file we need to
	    // manually revoke the object URL to avoid memory leaks.
	    if (textFile !== null) {
	      window.URL.revokeObjectURL(textFile);
	    }
	    textFile = window.URL.createObjectURL(data);

	    let element = document.createElement('a');
	    let name = "tree_" + this.graphData[type].output.type + '.gv';
	    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(this.graphData[type].output.data));
	    element.setAttribute('download', name);

	    element.style.display = 'none';
	    document.body.appendChild(element);

	    element.click();

	    document.body.removeChild(element);
	},
	genRandom: function(event) {
 		 this.loading = true;
 		 axios
     		.get(VUE_APP_BASE_API_URL + 'treegenerator/' + this.noOfNodes)
     		.then((response) => {
     			this.loading = false;
     			this.graphData[type] = { output: {
     					type: type, 
     					data: response.data, 
     					time: new Date(), 
     					params: { noOfNodes: this.noOfNodes},
     					viz: response.data
     				}
     			};
     			localStorage.graphData = JSON.stringify(this.graphData);
     		}, (error) => (this.loading = false));
   	 	stop = true;
     	d3.select("#animate").html("");	
	},
	genPattern: function(event) {
 		 this.loading = true;
 		 axios
     		.get(VUE_APP_BASE_API_URL + 'treegenerator/pattern/' + this.noOfNodes, { 
     				params : { pattern: this.patternToArray(this.pattern) },
     				paramsSerializer: params => {
     				    return jQuery.param(params, true)
     				  }
     			})
     		.then((response) => {
     			this.loading = false;
     			
     			this.graphData[type] = { output: {
 					type: type, 
 					data: response.data, 
 					time: new Date(), 
 					params: { 
 						noOfNodes: this.noOfNodes, 
 						noOfLevels: this.noOfLevels,
 						pattern: this.pattern
 						},
 					viz: response.data
     			}};
     			localStorage.graphData = JSON.stringify(this.graphData);
     			
     		}, (error) => (this.loading = false));
 		 stop = true;
 		 d3.select("#animate").html("");	
   },
   distort: function(event) {
  		 this.loading = true;
  		 let formData = new FormData();
  		 //formData.append('file', this.file);
  		 formData.append('file', this.graphData[type].input.data);
    	 axios
      		.post(VUE_APP_BASE_API_URL + 'treegenerator/distort/' + this.modified + '/' + this.added + '/' + this.deleted,
      			formData,
      			  {
      			    headers: {
      			        'Content-Type': 'multipart/form-data'
      			    }
      			  })
      		.then((response) => {
      			this.loading = false;
      			this.graphData[type].output =  {
 					type: type, 
 					data: response.data, 
 					time: new Date(), 
 					params: { 
 						added: this.added, 
 						deleted: this.deleted,
 						modified: this.modified
 						},
 					viz: response.data
     			};
     			localStorage.graphData = JSON.stringify(this.graphData);

      		}, (error) => (this.loading = false));
    	stop = true;
      	d3.select("#animate").html("");	
    },
    animate: function(event) {
 		 this.loading = true;
 		 stop = true;
   	 	 axios
  
     		.get(VUE_APP_BASE_API_URL + 'treegeneratoranimate/' + this.noOfNodes)
     		.then((response) => {
     			
     			this.loading = false;
     			this.generateData = "";
     			
     			var dots = response.data.dots;	
     			var dotIndex = 0;
     			d3.select("#animate").html("");
     			this.graphData = dots[dots.length-1]; //the last dot is the final graph
     			stop = false;
     			var graphviz = d3.select("#animate").graphviz()
     			    .transition(function () {
     			        return d3.transition("main")
     			            .ease(d3.easeLinear)
     			            .delay(500)
     			            .duration(1500);
     			    })
     			    .logEvents(true)
     			    .on("initEnd", render);
				
     			function render() {
     			    //var dotLines = dots[dotIndex];
     			    //var dot = dotLines.join('');
     			    if(stop){
     			    	return;
     			    }
     			    var dot = dots[dotIndex];
     			    graphviz
     			        .renderDot(dot)
     			        .on("end", function () {
     			            //dotIndex = (dotIndex + 1) % dots.length;
     			            dotIndex++;
     			            if(dotIndex < dots.length) 
     			            	render();
     			        });
     			}
     		}, (error) => (this.loading = false))
   },
   initializePattern(n){
	   this.pattern = [];
	   var val = 100/this.noOfLevels;
	   for(var i=0; i < this.noOfLevels; i++){
		   this.pattern.push({id: i, value: val});
	   }
	   Vue.nextTick()
	   .then(function () {
		   $('[data-toggle="tooltip"]').tooltip();
	   });
   },
   noOfLevelsChanged: function(n, event){
	   this.initializePattern(n);
   },
   patternSum: function(){
   	var sum = 0;
   	this.pattern.forEach(function(level) {
   		sum += level.value;
   	});
   	return sum;
   },
   patternToArray: function(pattern){
	   let res = [];
	   for(let i = 0; i < pattern.length; i++){
		   res.push(pattern[i].value);
	   }
	   return res;
   },
   sliderChange: function(item, event){
	   var delta = event.target.value - item.value;
	   var sum = this.patternSum();
	   
	   //if(sum + delta <= 100 && this.pattern[item.id-1].value != 0){
		   if(sum + delta <= 100){
		   item.value = parseFloat(event.target.value);
		   //event.target.value = item.value.toString();
	   } else {
		   event.target.value = item.value;
	   }
   },
   layoutChange: function(event){
	   let newL = event.target.value;
	   let oldL = newL == 'twopi' ? 'dot' : 'twopi'; 
	   
	   if(this.graphData[type] != undefined){
		   let data = this.graphData[type];
		   if(data.input != undefined){
			   data.input.viz = this.addOrChangeLayout(data.input.viz, oldL, newL);
			   data.input.data = this.addOrChangeLayout(data.input.data, oldL, newL);
			   //data.input.viz.replace('layout=' + oldL + ';', 'layout=' + newL +';')
			   //data.input.data.replace('layout=' + oldL + ';', 'layout=' + newL +';')
		   }
		   if(data.output != undefined){
			   data.output.viz = this.addOrChangeLayout(data.output.viz, oldL, newL);
			   data.output.data = this.addOrChangeLayout(data.output.data, oldL, newL);
			   //data.output.viz.replace('layout=' + oldL + ';', 'layout=' + newL +';')
			   //data.output.data.replace('layout=' + oldL + ';', 'layout=' + newL +';')
		   }
	   }
   },
   addOrChangeLayout: function(data, oldL, newL){
	   if(data.match('layout=') != undefined){
		   data = data.replace('layout=' + oldL + ';', 'layout=' + newL +';');
	   } else {
		   data = data.substring(0, data.length-1).concat('layout=' + newL + ';\n}');
	   }
	   return data;
   },
   getParams: function(data){
	   let res = '';
	   if(data != undefined && data.params != undefined){
		   for (let param in data.params) {
			  if(param == 'pattern'){
				  res += ' ' + param + ': ' + this.patternToArray(data.params[param]).toString();
			  } else {
				  res += ' ' + param + ': ' + data.params[param];
			  }
		   }
	   }
	   return res;
   },
   goDistort: function(event){
	   this.graphData['distort'] = { input : this.graphData[type].output, output: null };
	   localStorage.graphData = JSON.stringify(this.graphData);
	   //this.$router.push('/distort.html');
	   window.location.href = VUE_APP_BASE_URL + '/distort.html';
   },
   addSvgPlugin: function(output){
	   let type = output ? 'output' : 'input';
	   Vue.nextTick()
	   .then(function () {
		   $(function() {
			   panZoomInstance = svgPanZoom('#tree-' + type + ' svg', {
			     zoomEnabled: true,
			     controlIconsEnabled: false,
			     fit: true,
			     center: true,
			     minZoom: 0.1
			   });
			   
			   // zoom out
			   //panZoomInstance.zoom(0.2)

			   $("#move").on("click", function() {
			     // Pan by any values from -80 to 80
			     panZoomInstance.panBy({x: Math.round(Math.random() * 160 - 80), y: Math.round(Math.random() * 160 - 80)})
			   });
			 })
	   })
   },
   toggleHelp: function(event){
	   $('[data-toggle="tooltip"]').tooltip('toggleEnabled');
	   if(this.graphData.help){
		   $(".fa-question-circle").addClass("fa-question-circle-o").removeClass("fa-question-circle");
		   this.graphData.help = false;
	   } else {
		   $(".fa-question-circle-o").addClass("fa-question-circle").removeClass("fa-question-circle-o");
		   this.graphData.help = true;
	   }
	   $(event.target).tooltip('dispose');
	   Vue.nextTick().then(function () {
		   $(event.target).tooltip();
	   });
	   //
	   
	   localStorage.graphData = JSON.stringify(this.graphData);
   }
  }
});

Vue.nextTick(function () {
	if(app.graphData.help){
		$(".fa-question-circle-o").addClass("fa-question-circle").removeClass("fa-question-circle-o");
	} else {
		$('[data-toggle="tooltip"]').tooltip('disable');
		$(".fa-question-circle-o").tooltip('enable');
	}
});

$(function () {
	  $('[data-toggle="tooltip"]').tooltip();
	});