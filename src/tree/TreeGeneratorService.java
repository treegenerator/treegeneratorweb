package tree;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.core.header.FormDataContentDisposition;

import classhier.graph.*;
import treegenerator.algorithms.GenerateRandom;
import treegenerator.algorithms.DistortTree;
import treegenerator.algorithms.GenParams;
import treegenerator.algorithms.GeneratePattern;

@Path("/treegenerator")
public class TreeGeneratorService {
	
	@GET
	@Produces("text/plain")
	public String generateRandom() {
		int n = 20;
		
		Tree t = new GenerateRandom(new GenParams(n)).generate();
		
		String result = "@Produces(\"text/plaing\") Output: \n\n Random generated tree " + n;
		return GraphImportExport.exportToDot(t, false, false, Layout.dot).toString();
		
	}
	@Path("/{n}")
	@GET
	@Produces("text/plain")
	public String generateRandom(@PathParam("n") Integer n) {
		
		Tree t = new GenerateRandom(new GenParams(n)).generate();
		
		String result = "@Produces(\"text/plaing\") Output: \n\n Random generated tree";
		return GraphImportExport.exportToDot(t, false, false, Layout.dot).toString();
	}
	
	
	@Path("/pattern/{n}")
	@GET
	@Produces("text/plain")
	public String generateRandomPattern(@PathParam("n") Integer n, @QueryParam("pattern") final List<Float> pattern) {
		float[] floatArray = new float[pattern.size()];
		int i = 0;

		for (Float f : pattern) {
		    floatArray[i++] = (f != null ? f/100 : Float.NaN); // Or whatever default you want.
		}
		GenParams gp = new GenParams(n, floatArray);

		Tree t = new GeneratePattern(gp).generate();
		
		String result = "@Produces(\"text/plaing\") Output: \n\n Random generated tree";
		return GraphImportExport.exportToDot(t, false, false, Layout.dot).toString();
	}
	
	@Path("/distort/{m}/{a}/{d}")
	//@Path("/distort")
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces("text/plain")
	public String distort(@PathParam("m") Float m, @PathParam("a") Integer a, @PathParam("d") Integer d,
		@FormDataParam("file") InputStream uploadedInputStream,
	    @FormDataParam("file") FormDataContentDisposition fileDetails) {
		
	   Tree inputTree = GraphImportExport.loadTreeFromStream(uploadedInputStream);
	   GenParams gp = new GenParams(inputTree, a, d, m);
	   
	   Tree outputTree = new DistortTree(gp).generate();
	   
	   String result = "@Produces(\"text/plaing\") Output: \n\n Distorted tree";
	   return GraphImportExport.exportToDot(outputTree, false, false, Layout.dot).toString();
	}
	
}
